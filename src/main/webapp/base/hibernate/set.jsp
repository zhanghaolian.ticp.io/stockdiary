<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	
</head>

<body>
	<a id="btn" href="#" class="easyui-linkbutton" onclick="ab()">清空Hibernate缓存</a>
	<a id="btn" href="#" class="easyui-linkbutton" onclick="reload()">刷新Hibernate配置</a>
</body>
</html>

<script>
	function ab(){
		$.post( plat.fullUrl("/base/hibernate/buffer.do") );
	}
	
	function reload(){
		$.post( plat.fullUrl("/base/hibernate/reloadConfig.do") );
	}
</script>
