/**
 * easyui扩展
 */
(function($) {
	$.extend($.fn.form.methods, {
		/**
		 * getData 获取数据接口
		 * 
		 * @param {Object} jq
		 * @param {Object} params 设置为true的话，会把string型"true"和"false"字符串值转化为boolean型。
		 */
	    getData: function(jq, field){
	        var formArray = jq.serializeArray();
	        var oRet = {};
	        for (var i in formArray) {
	            if (typeof(oRet[formArray[i].name]) == 'undefined') 
	            	oRet[formArray[i].name] = formArray[i].value;
	            else  oRet[formArray[i].name] += "," + formArray[i].value;
	        }
	        if(field){
	        	return oRet[field];
	        }
	        return oRet;
	    },
	    
	    /**
	     * ajaxSubmit
	     * 
	     */
	    ajaxSubmit: function(target,params){
	    	if(params.validate!="false"&&params.validate!=false){
				var v = target.form('validate');
				if (!v) return false;
	    	}
	    	if(params.onSubmit&&(typeof params.onSubmit)=="function"){
	    		var v=params.onSubmit();
	    		if (!v) return false;
	    	}
	    	if(params.mask!="false"&&params.mask!=false){
	    		$.messager.progress();
	    	}
	    	var data=target.form("getData");
	    	$.extend(data,params.data);
			$.post(params.url,
				data, 
				function(data) {
			    	if(params.mask!="false"&&params.mask!=false){
			    		$.messager.progress("close");
			    	}
			    	if(params.success&& $.type(params.success)==="function"){
			    		params.success(data);
			    	}
				},
				"json"
			); 
	    }
	});
	
	
	var dateboxparse=$.fn.datebox.defaults.parser;
	
	$.extend($.fn.datebox.defaults, {
		parser: function(s){
			if (!s)
				return new Date();
			if($.type(s) === "number"){
				return new Date(s)
			}else{
				return dateboxparse(s);
			}
		}
	});
	
	$.extend($.fn.datetimebox.defaults, {
		parser: function(s){
			if (!s)
				return new Date();
			if($.type(s) === "number"){
				return new Date(s)
			}else{
				var dt=s.split(" ");
				var d=dateboxparse(dt[0]);
				if(dt.length<2){
				return d;
				}
				var _2d=$(this).datetimebox("spinner").timespinner("options").separator;
				var tt=dt[1].split(_2d);
				var _2e=parseInt(tt[0],10)||0;
				var _2f=parseInt(tt[1],10)||0;
				var _30=parseInt(tt[2],10)||0;
				return new Date(d.getFullYear(),d.getMonth(),d.getDate(),_2e,_2f,_30);
			}
		}
	});
	
})(jQuery);