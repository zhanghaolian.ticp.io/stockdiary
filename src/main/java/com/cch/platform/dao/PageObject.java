package com.cch.platform.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分页对象. 包含当前页数据及分页信息如总记录数.
 *
 */
@SuppressWarnings("rawtypes")
public class PageObject implements Serializable {

	private static final long serialVersionUID = -5161893895307319978L;


	private long total;  // 总记录数


	private List rows;  // 当前页中存放的记录,类型一般为List
	
	private int pageNo;
	
	private int pageSize;

	/**
	 * 构造方法，只构造空页.
	 */
	public PageObject() {
	} 
	
	public PageObject(int pageNo,int pageSize){
		this.pageNo=pageNo;
		this.pageSize=pageSize;
		this.rows=new ArrayList();
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List getRows() {
		return rows;
	}

	public void setRows(List rows) {
		this.rows = rows;
	}

	public int starIndex() {
		return pageNo*pageSize-pageSize;
	}
	
	public Map<String,Object> getModel(){
		Map<String,Object> re=new HashMap<String,Object>();
		re.put("total", total);
		if(rows!=null){
			re.put("rows",rows);
		}
		return re;
	}
}