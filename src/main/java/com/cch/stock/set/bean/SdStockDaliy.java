package com.cch.stock.set.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * SdStockDaliy entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sd_stock_daliy")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE,region="com.cch.stock.set.bean.SdStockDaliy")
public class SdStockDaliy implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7038551450217594410L;
	// Fields

	private String stockCode;
	private Date reDate;
	private Double priceOpen;
	private Double priceClose;
	private Double priceHigh;
	private Double priceLow;
	private Long volume;

	// Constructors

	/** default constructor */
	public SdStockDaliy() {
	}

	/** minimal constructor */
	public SdStockDaliy(String stockCode, Date reDate) {
		this.stockCode = stockCode;
		this.reDate= reDate;
	}

	/** full constructor */
	public SdStockDaliy(String stockCode, Date reDate, Double priceOpen, Double priceClose,
			Double priceHigh, Double priceLow, Long volume) {
		this.stockCode = stockCode;
		this.reDate= reDate;
		this.priceOpen = priceOpen;
		this.priceClose = priceClose;
		this.priceHigh = priceHigh;
		this.priceLow = priceLow;
		this.volume = volume;
	}

	// Property accessors
	@Id
	@Column(name = "stock_code", nullable = false, length = 10)
	public String getStockCode() {
		return this.stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	@Id
	@Temporal(TemporalType.DATE)
	@Column(name = "re_date", nullable = false, length = 10)
	public Date getReDate() {
		return this.reDate;
	}

	public void setReDate(Date reDate) {
		this.reDate = reDate;
	}

	@Column(name = "price_open", precision = 10, scale = 3)
	public Double getPriceOpen() {
		return this.priceOpen;
	}

	public void setPriceOpen(Double priceOpen) {
		this.priceOpen = priceOpen;
	}

	@Column(name = "price_close", precision = 10, scale = 3)
	public Double getPriceClose() {
		return this.priceClose;
	}

	public void setPriceClose(Double priceClose) {
		this.priceClose = priceClose;
	}

	@Column(name = "price_high", precision = 10, scale = 3)
	public Double getPriceHigh() {
		return this.priceHigh;
	}

	public void setPriceHigh(Double priceHigh) {
		this.priceHigh = priceHigh;
	}

	@Column(name = "price_low", precision = 10, scale = 3)
	public Double getpriceLow() {
		return this.priceLow;
	}

	public void setpriceLow(Double priceLow) {
		this.priceLow = priceLow;
	}

	@Column(name = "volume", precision = 16, scale = 0)
	public Long getVolume() {
		return this.volume;
	}

	public void setVolume(Long volume) {
		this.volume = volume;
	}
	
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SdStockDaliy))
			return false;
		SdStockDaliy castOther = (SdStockDaliy) other;
		return ((this.getStockCode() == castOther.getStockCode()) || (this
				.getStockCode() != null && castOther.getStockCode() != null && this
				.getStockCode().equals(castOther.getStockCode())))
				&& ((this.getReDate() == castOther.getReDate()) || (this
						.getReDate() != null && castOther.getReDate() != null && this
						.getReDate().equals(castOther.getReDate())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getStockCode() == null ? 0 : this.getStockCode().hashCode());
		result = 37 * result
				+ (getReDate() == null ? 0 : this.getReDate().hashCode());
		return result;
	}
}